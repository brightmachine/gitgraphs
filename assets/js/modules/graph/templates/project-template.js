'use strict';

//require gitgraph instance
require('gitgraph.js');

/**
 * Generate the template theme and intialise + return GitGraph
 * @returns {GitGraph}
 */
module.exports = function() {

    let myTemplateConfig = {
        colors: [ "#658C9D", "#B3997E", "#362717", "#556E73" ], // branches colors, 1 per column
        branch: {
            lineWidth: 8,
            spacingX: 50
        },
        commit: {
            spacingY: -60,
            dot: {
                size: 10
            },
            message: {
                displayAuthor: true,
                displayBranch: true,
                displayHash: false,
                font: "normal 12pt Arial"
            },
            shouldDisplayTooltipsInCompactMode: false, // default = true
            tooltipHTMLFormatter: function ( commit ) {
                return "<b>" + commit.sha1 + "</b>" + ": " + commit.message;
            }
        }
    };

    let myTemplate = new GitGraph.Template( myTemplateConfig );


    /***********************
     *    INITIALIZATION   *
     ***********************/

    var config = {
        template: myTemplate      // could be: "blackarrow" or "metro" or `myTemplate` (custom Template object)
        //, reverseArrow: true  // to make arrows point to ancestors, if displayed
        //, orientation: "vertical-reverse"
        //, mode: "compact"     // special compact mode : hide messages & compact graph
    };

    return new GitGraph(config);

}
