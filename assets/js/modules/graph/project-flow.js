'use strict';


/**
 * Require template for graph, return graph object instance of gitgraph.js http://gitgraphjs.com/
 * @type {exports}
 */
const GraphTemplate = require('./templates/project-template');


/**
 *   Generates all of the 'project flow' Git model diagram
 *   @documentation http://gitgraphjs.com/#detail-base
 */
const projectFlow =  function() {

    let graph = new GraphTemplate;

    /***********************
     * BRANCHS AND COMMITS *
     ***********************/


    // Create branch named "master"
    var master = graph.branch("master");

    // Commit on HEAD Branch which is "master"
    master.commit({
        dotSize: 10,
        message: "Forked from origin to local repo",
        author: "Phill <phill@developer.io>",
        onClick: function(commit) {
            console.log("Oh, you clicked my commit?!", commit);
        }
    });

    var dev = graph.branch("dev");

    // Commit on HEAD Branch which is "master"
    dev.commit({
        dotSize: 10,
        message: "Created dev branch",
        author: "Phill <phill@developer.io>",
        onClick: function(commit) {
            console.log("Oh, you clicked my commit?!", commit);
        }
    });


    var feature = dev.branch("feature-137");


    // Commit on HEAD Branch which is "master"
    feature.commit({
        dotSize: 10,
        message: "Created feature branch relating to JIRA issue",
        author: "Phill <phill@developer.io>",
        onClick: function(commit) {
            console.log("Oh, you clicked my commit?!", commit);
        }
    });



    // Commit on HEAD Branch which is "master"
    feature.commit({
        dotSize: 10,
        message: "Working on feature branch",
        author: "Phill <phill@developer.io>",
        onClick: function(commit) {
            console.log("Oh, you clicked my commit?!", commit);
        }
    });

    // Commit on HEAD Branch which is "master"
    feature.commit({
        dotSize: 10,
        message: "Working on feature branch",
        author: "Phill <phill@developer.io>",
        onClick: function(commit) {
            console.log("Oh, you clicked my commit?!", commit);
        }
    });


    feature.merge(dev, 'Destroy feature branch and merge back into dev');

    var release = dev.branch("release");

    // Commit on HEAD Branch which is "master"
    release.commit({
        dotSize: 10,
        message: "Created release branch for approval",
        author: "Phill <phill@developer.io>",
        onClick: function(commit) {
            console.log("Oh, you clicked my commit?!", commit);
        }
    });

    // Commit on HEAD Branch which is "master"
    dev.commit({
        dotSize: 10,
        message: "Some work after release",
        author: "Phill <phill@developer.io>",
        onClick: function(commit) {
            console.log("Oh, you clicked my commit?!", commit);
        }
    });


    release.merge(dev, 'Destroy release branch and merge back into dev');
    release.merge(master, 'Destroy release branch and merge back into master');


};


module.exports = projectFlow;
